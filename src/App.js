import './style/App.css';
import './index.css';
import useContenful from './hooks/useContenful';
import { useEffect, useState } from 'react';
function App() {
  const [projects, setProjects] = useState([]);
  const { getProjects } = useContenful();

  useEffect(() => {
    getProjects().then((response) => console.log(response));
  }, []);
  
  return (
    <div className="App bg-slate-500">
    </div>
  );
}

export default App;
