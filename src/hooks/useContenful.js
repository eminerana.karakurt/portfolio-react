import { createClient } from 'contentful';

const useContenful = () => {
    const client = createClient({
        space: "l657876t4eub",
        accessToken: "wy40npmgT6aCSt6PurUmxXF__Mm3n9nKkuMActiloj8",
        host: "preview.contentful.com"
    });
    const getProjects = async () => {
        try {
            const entries = client.getEntries({
                content_type: "projet",
                select: "fields",
            });
            return entries;
        } catch (error) {
            console.log(error);
        }
    };
    return { getProjects };
};

export default useContenful;